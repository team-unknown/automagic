import baseCreateDebug from 'debug';
import { REDUX_NAMESPACE } from '../constants';

export default function createDebug(ns) {
  return baseCreateDebug(`${REDUX_NAMESPACE}:${ns}`);
}
