import { eventChannel } from 'redux-saga';

export default function createTimerChannel(ms) {
  return eventChannel(emitter => {
    function onTick() {
      emitter(new Date());
    }

    let interval;

    function subscribe() {
      interval = setInterval(onTick, ms);
    }
    function unsubscribe() {
      if (interval) {
        clearInterval(interval);
      }
    }

    subscribe();

    return unsubscribe;
  });
}
