import { REDUX_NAMESPACE } from '~app/constants';

// eslint-disable-next-line import/prefer-default-export
export const getAppState = state => state[REDUX_NAMESPACE];

export { default as createEventChannel } from './createEventChannel';
export { default as createTimerChannel } from './createTimerChannel';

export * from './array';
