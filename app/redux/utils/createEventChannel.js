import { eventChannel } from 'redux-saga';

export default function createEventChannel(eventSource, eventName) {
  return eventChannel(emitter => {
    function onMessage(msg) {
      emitter(msg);
    }

    function subscribe() {
      eventSource.on(eventName, onMessage);
    }
    function unsubscribe() {
      eventSource.off(eventName, onMessage);
    }

    subscribe();

    return unsubscribe;
  });
}
