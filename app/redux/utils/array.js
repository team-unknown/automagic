export function arrayAdd(array, payload) {
  if (typeof payload.index === 'undefined') {
    return [...array, payload.item];
  }

  return [
    ...array.slice(0, payload.index),
    payload.item,
    ...array.slice(payload.index),
  ];
}

export function arrayUpdate(array, payload) {
  return array.map((item, index) => {
    if (index !== payload.index) {
      // This isn't the item we care about - keep it as-is
      return item;
    }

    // Otherwise, this is the one we want - return an updated value
    return {
      ...item,
      ...payload.item,
    };
  });
}

export function arrayRemove(array, payload) {
  return [...array.slice(0, payload.index), ...array.slice(payload.index + 1)];
}
