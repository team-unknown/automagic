import { all } from 'redux-saga/effects';

import * as global from './global/saga';
import * as hotkey from './hotkey/saga';

const debug = require('~debug')('rootSaga');

export default function* rootSaga() {
  debug('initializing sagas');

  try {
    yield all([...global.init, ...hotkey.init]);

    debug('loading all sagas');
    yield all([...global.load, ...hotkey.load]);
    debug('loaded all sagas');
  } catch (err) {
    debug('got error during saga initialization. Error: %s', err.stack);
    // TODO: handle error using redux dispatch
  }

  debug('initialized sagas');
}
