// @flow

import _ from 'lodash';
import * as actions from './actions';
import type { Hotkey } from './actions';

export default function hotkey(
  state: { [hotkey_id: string]: Hotkey } = {},
  action
) {
  switch (action.type) {
    case actions.INSERT_HOTKEY:
      return {
        ...state,
        [action.payload.item.id]: action.payload.item,
      };
    case actions.UPDATE_HOTKEY:
      return {
        ...state,
        [action.payload.item.id]: {
          ...state[action.payload.item.id],
          ...action.payload.item,
        },
      };
    case actions.DELETE_HOTKEY:
      return _.omitBy(state, item => action.payload.item.id === item.id);
    default:
      return state;
  }
}
