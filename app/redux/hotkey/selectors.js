import { createSelector } from 'reselect';
import _ from 'lodash';

import { MODULE_NAME } from './constants';
import { getAppState } from '../utils';

export const selectModule = state => getAppState(state)[MODULE_NAME];
export default selectModule;

export const selectHotkeys = createSelector([selectModule], module =>
  _.toArray(module)
);

export const selectHotkey = createSelector(
  [selectModule, (state, id) => id],
  (module, id) => module[id]
);
