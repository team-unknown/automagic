// @flow

import { REDUX_NAMESPACE } from '~app/constants';
import { MODULE_NAME } from './constants';

export const INSERT_HOTKEY = `${REDUX_NAMESPACE}/${MODULE_NAME}/INSERT_HOTKEY`;
export const UPDATE_HOTKEY = `${REDUX_NAMESPACE}/${MODULE_NAME}/UPDATE_HOTKEY`;
export const DELETE_HOTKEY = `${REDUX_NAMESPACE}/${MODULE_NAME}/DELETE_HOTKEY`;

export interface Hotkey {
  id?: ?string;
  enabled?: ?boolean;
  title?: ?string;
  key?: ?string;
  code?: ?string;
}

export const insertHotkey = (item: Hotkey) => ({
  type: INSERT_HOTKEY,
  payload: {
    item,
  },
});

export const updateHotkey = (item: Hotkey) => ({
  type: UPDATE_HOTKEY,
  payload: {
    item,
  },
});

export const deleteHotkey = (item: Hotkey) => ({
  type: DELETE_HOTKEY,
  payload: {
    item,
  },
});
