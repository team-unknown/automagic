import { takeLatest } from 'redux-saga';
import { fork, select, all, call, put } from 'redux-saga/effects';
import * as electron from 'electron';
import vm from 'vm';
import _ from 'lodash';
import { push } from 'react-router-redux';
import activeWin from 'active-win';
import robot from 'robotjs';

import * as actions from './actions';
import * as selectors from './selectors';

const debug = require('~debug')('redux:global:saga');

const defaultContext = {
  console,
  setTimeout,
  setInterval,
  electron,
  activeWin,
  robot,
  action() {
    console.log('empty action');
  },
};

const hotkeyStore = {};

function unregisterHotkey(hotkey) {
  electron.remote.globalShortcut.unregister(hotkey.key);
}

function registerHotkey(hotkey) {
  if (!hotkey.enabled || _.isEmpty(hotkey.key) || _.isEmpty(hotkey.code)) {
    return hotkey;
  }

  const context = {
    ...defaultContext,
  };

  vm.runInNewContext(hotkey.code, context);

  electron.remote.globalShortcut.register(hotkey.key, async () => {
    console.log('hotkey called', hotkey);
    if (_.isFunction(context.action)) {
      await context.action();
    }
  });

  return {
    ...hotkey,
    context,
  };
}

function* updateHotkey(hotkey) {
  try {
    let registeredHotkey = hotkeyStore[hotkey.id];
    if (!registeredHotkey) {
      registeredHotkey = yield call(registerHotkey, hotkey);
    } else if (
      registeredHotkey.enabled !== hotkey.enabled ||
      registeredHotkey.key !== hotkey.key ||
      registeredHotkey.code !== hotkey.code
    ) {
      yield call(unregisterHotkey, registeredHotkey);
      registeredHotkey = yield call(registerHotkey, hotkey);
    }

    hotkeyStore[hotkey.id] = {
      ...registeredHotkey,
      ...hotkey,
    };
  } catch (err) {
    debug('error while updating hotkey: %s', err.stack);
  }
}

function* updateHotkeys() {
  const hotkeys = yield select(selectors.selectHotkeys);

  debug('updating all hotkeys');
  yield all(hotkeys.map(hotkey => call(updateHotkey, hotkey)));
  debug('updated all hotkeys');
}

function* redirect(action) {
  debug('redirecting');
  if (action.type === actions.INSERT_HOTKEY) {
    yield put(push(`/hotkeys/${action.payload.item.id}`));
  } else if (action.type === actions.DELETE_HOTKEY) {
    yield put(push('/hotkeys'));
  }
  debug('redirected');
}

function* unregister(action) {
  debug('unregistering deleted key');
  yield call(updateHotkey, {
    ...action.payload.item,
    enabled: false,
  });
  debug('unregistered deleted key');
}

function* start() {
  yield takeLatest([actions.DELETE_HOTKEY], unregister);
  yield takeLatest([actions.INSERT_HOTKEY, actions.DELETE_HOTKEY], redirect);
  yield takeLatest(
    [actions.INSERT_HOTKEY, actions.UPDATE_HOTKEY, actions.DELETE_HOTKEY],
    updateHotkeys
  );
}

export const init = [fork(start)];
export const load = [];
