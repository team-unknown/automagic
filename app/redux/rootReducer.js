// @flow
import { combineReducers } from 'redux';
import { reducer as routerReducer } from './router';
import { REDUX_NAMESPACE } from '../constants';

import global from './global/reducer';
import hotkey from './hotkey/reducer';

const reducers = combineReducers({
  global,
  hotkey,
});

export default combineReducers({
  [REDUX_NAMESPACE]: reducers,
  router: routerReducer,
});
