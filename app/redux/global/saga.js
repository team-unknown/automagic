import { delay } from 'redux-saga';
import { fork } from 'redux-saga/effects';

const debug = require('~debug')('redux:global:saga');

function* start() {
  debug('initializing');
  yield delay(1000);
  debug('initialized');
}

export const init = [fork(start)];
export const load = [];
