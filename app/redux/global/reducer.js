import { combineReducers } from 'redux';

import actions from './actions';

function sidebar(state = { collapsed: false }, action) {
  switch (action.type) {
    case actions.TOGGLE_SIDEBAR:
      return {
        ...state,
        collapsed: !state.collapsed,
        test: 'asdasd',
      };
    default:
      return state;
  }
}

export default combineReducers({
  sidebar,
});
