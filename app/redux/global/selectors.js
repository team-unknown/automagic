import { createSelector } from 'reselect';

import { MODULE_NAME } from './constants';
import { getAppState } from '../utils';

export const selectModule = state => getAppState(state)[MODULE_NAME];
export default selectModule;

export const selectSidebar = createSelector(
  [selectModule],
  module => module.sidebar
);
