import { REDUX_NAMESPACE } from '~app/constants';
import { MODULE_NAME } from './constants';

const TOGGLE_SIDEBAR = `${REDUX_NAMESPACE}/${MODULE_NAME}/TOGGLE_SIDEBAR`;

export const types = {
  TOGGLE_SIDEBAR,
};
export default types;

export const toggleSidebar = () => ({
  type: TOGGLE_SIDEBAR,
  payload: {},
});
