/* eslint-disable global-require */
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import { createLogger } from 'redux-logger';

import { middleware as routerMiddleware } from './router';
import { REDUX_NAMESPACE } from '../constants';
import sagaManager from './sagaManager';
import rootReducer from './rootReducer';

const sagaMiddleware = createSagaMiddleware();

// eslint-disable-next-line import/prefer-default-export
export function configureStore(
  initialState = { [REDUX_NAMESPACE]: {}, router: {} }
) {
  const logger = createLogger({
    level: 'info',
    collapsed: true,
  });

  const store = createStore(
    rootReducer,
    initialState,
    composeWithDevTools(
      applyMiddleware(sagaMiddleware, routerMiddleware, logger)
    )
  );

  global.store = store;

  sagaManager.startSagas(sagaMiddleware);

  if (module.hot) {
    module.hot.accept('./rootReducer', () =>
      store.replaceReducer(require('./rootReducer').default)
    );

    module.hot.accept('./sagaManager', () => {
      sagaManager.cancelSagas(store);
      require('./sagaManager').default.startSagas(sagaMiddleware);
    });
  }

  return store;
}
