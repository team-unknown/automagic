/* eslint flowtype-errors/show-errors: 0 */
import React from 'react';
import { Switch, Route, Redirect } from 'react-router';
import AppBar from '@material-ui/core/AppBar';
import Drawer from '@material-ui/core/Drawer';
import { withStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';

import App from './components/App';
import HotkeyLayout from './components/HotkeyLayout';
import HotkeyList from './components/HotkeyList';

const drawerWidth = 240;

const styles = theme => ({
  root: {
    flexGrow: 1,
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawerPaper: {
    position: 'relative',
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
    minWidth: 0, // So the Typography noWrap works
  },
  toolbar: theme.mixins.toolbar,
  link: {
    textDecoration: 'none',
    color: 'white',
  },
});

function MainLayout(props) {
  const { classes } = props;
  return (
    <App>
      <div className={classes.root}>
        <AppBar position="fixed" className={classes.appBar}>
          <Toolbar>
            <Typography variant="title" color="inherit" noWrap>
              <Link className={classes.link} to="/hotkeys">
                Automagic
              </Link>
            </Typography>
          </Toolbar>
        </AppBar>
        <Drawer
          variant="permanent"
          classes={{
            paper: classes.drawerPaper,
            docked: 'docked',
          }}
        >
          <div className={classes.toolbar} />
          <Switch>
            <Route path="/hotkeys" component={HotkeyList} />
          </Switch>
        </Drawer>
        <main className={classes.content}>
          <div className={classes.toolbar} />

          <Switch>
            <Route exact path="/" render={() => <Redirect to="/hotkeys" />} />
            <Route path="/hotkeys" component={HotkeyLayout} />
          </Switch>
        </main>
      </div>
    </App>
  );
}

export default withStyles(styles)(MainLayout);
