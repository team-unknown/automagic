import React from 'react';
import { Route } from 'react-router';
import HotkeyPage from './HotkeyPage';
import HotkeyForm from './HotkeyForm';

export default () => (
  <div>
    <Route path="/hotkeys" exact component={HotkeyPage} />
    <Route path="/hotkeys/:id" component={HotkeyPage} />
    <Route path="/hotkeys/:id" component={HotkeyForm} />
  </div>
);
