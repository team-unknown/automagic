import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import { withStyles } from '@material-ui/core/styles';
import KeyboardHideIcon from '@material-ui/icons/KeyboardHide';
import ErrorIcon from '@material-ui/icons/Error';
import { connect } from 'react-redux';
import { compose } from 'recompose';

import * as hotkeyActions from '~app/redux/hotkey/actions';
import * as hotkeySelectors from '~app/redux/hotkey/selectors';
import HotkeyRecorder from './HotkeyRecorder';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  textField: {
    width: '100%',
  },
  icon: {
    marginRight: theme.spacing.unit,
  },
  grid: {
    marginBottom: '30px',
  },
  button: {
    width: '100px',
  },
});

const DEFAULT_STATE = {
  isRecording: false,
};

class HotkeyForm extends Component {
  constructor(props) {
    super(props);
    this.state = DEFAULT_STATE;
  }

  startRecording = () => {
    this.setState(prevState => ({
      ...prevState,
      isRecording: true,
    }));
  };
  stopRecording = () => {
    this.setState(prevState => ({
      ...prevState,
      isRecording: false,
    }));
  };

  renderRecorder() {
    if (this.state.isRecording) {
      return (
        <HotkeyRecorder
          onSave={keyComb => {
            this.props.onChange('key')({
              target: {
                value: keyComb,
              },
            });
            this.stopRecording();
          }}
          onDiscard={this.stopRecording}
        />
      );
    }

    return null;
  }

  render() {
    const { classes, item, onChange } = this.props;

    if (!item) {
      return null;
    }

    return (
      <div className={classes.root}>
        <Grid container spacing={24}>
          <Grid container className={classes.grid}>
            <Grid item xs={2}>
              <Switch
                checked={item.enabled}
                onChange={e =>
                  onChange('enabled')({ target: { value: e.target.checked } })
                }
                color="primary"
              />
            </Grid>
            <Grid item xs={10}>
              <TextField
                className={classes.textField}
                label="Title"
                value={item.title}
                helperText="Title of the hotkey"
                onChange={onChange('title')}
              />
            </Grid>
          </Grid>

          <Grid container className={classes.grid}>
            <Grid item xs={2}>
              <Button
                onClick={this.startRecording}
                variant="contained"
                color="primary"
                size="small"
                aria-label="Record"
                className={classes.button}
              >
                <KeyboardHideIcon className={classes.icon} />
                Record
              </Button>
              {this.renderRecorder()}
            </Grid>

            <Grid item xs={10}>
              <TextField
                className={classes.textField}
                disabled
                label="Hotkey"
                value={item.key}
                helperText="Key of the item"
              />
              {this.renderRecorder()}
            </Grid>
          </Grid>

          <Grid container className={classes.grid}>
            <Grid item xs={2}>
              <Button
                variant="contained"
                color="secondary"
                size="small"
                aria-label="Check"
                className={classes.button}
              >
                <ErrorIcon className={classes.icon} />
                Error
              </Button>
            </Grid>
            <Grid item xs={10}>
              <TextField
                className={classes.textField}
                multiline
                rows={11}
                label="Code"
                value={item.code}
                helperText="Key of the item"
                onChange={onChange('code')}
              />
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  item: hotkeySelectors.selectHotkey(state, props.match.params.id),
});

const mapDispatchToProps = (dispatch, props) => ({
  onChange: key => e => {
    const { id } = props.match.params;

    dispatch(
      hotkeyActions.updateHotkey({
        id,
        [key]: e.target.value,
      })
    );
  },
});

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(HotkeyForm);
