import React from 'react';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import SpeedDial from '@material-ui/lab/SpeedDial';
import SpeedDialAction from '@material-ui/lab/SpeedDialAction';
import SpeedDialIcon from '@material-ui/lab/SpeedDialIcon';
import uuid from 'uuid/v4';
import _ from 'lodash';

import * as hotkeyActions from '~app/redux/hotkey/actions';
import * as hotkeySelectors from '~app/redux/hotkey/selectors';

const styles = theme => ({
  speedDial: {
    position: 'absolute',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 3,
  },
});

class HotkeyPage extends React.Component {
  state = {
    open: false,
  };

  actions = [
    {
      icon: <AddIcon />,
      name: 'Create New Hotkey',
      handleClick: this.props.onAdd,
      needId: false,
    },
    {
      icon: <DeleteIcon />,
      name: 'Delete the Current Hotkey',
      handleClick: this.props.onDelete,
      needId: true,
    },
  ];

  onOpen = () => {
    this.setState({
      open: true,
    });
  };

  onClose = () => {
    this.setState({
      open: false,
    });
  };

  renderActions = () =>
    _.chain(this.actions)
      .filter(
        action =>
          !action.needId || (action.needId && this.props.match.params.id)
      )
      .map(action => (
        <SpeedDialAction
          key={action.name}
          icon={action.icon}
          tooltipTitle={action.name}
          onClick={() => action.handleClick(this.props.item)}
        />
      ))
      .value();

  render() {
    const { classes } = this.props;
    const { open } = this.state;

    return (
      <SpeedDial
        ariaLabel="Hotkey Options"
        className={classes.speedDial}
        icon={<SpeedDialIcon />}
        onClose={this.onClose}
        onMouseLeave={this.onClose}
        onMouseOver={this.onOpen}
        onFocus={this.onOpen}
        open={open}
      >
        {this.renderActions()}
      </SpeedDial>
    );
  }
}

const mapStateToProps = (state, props) => ({
  item: hotkeySelectors.selectHotkey(state, props.match.params.id),
});

const mapDispatchToProps = dispatch => ({
  onAdd() {
    const id = uuid();
    dispatch(
      hotkeyActions.insertHotkey({
        id,
        enabled: false,
        title: 'New Hotkey',
        key: 'Ctrl+Alt+Z',
        code: [
          'const win = electron.remote.getCurrentWindow();',
          '',
          'function action() {',
          '  if(win.isVisible()) {',
          '    win.hide();',
          '  } else {',
          '    win.show();',
          '  }',
          '}',
        ].join('\n'),
      })
    );
  },
  onDelete(hotkey) {
    dispatch(hotkeyActions.deleteHotkey(hotkey));
  },
});

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(HotkeyPage);
