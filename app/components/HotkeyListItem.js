import React from 'react';

import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import KeyboardIcon from '@material-ui/icons/Keyboard';
import { NavLink } from 'react-router-dom';
import { withStyles } from '@material-ui/core';
import indigo from '@material-ui/core/colors/indigo';

const styles = () => ({
  link: {
    textDecoration: 'none',
    backgroundColor: 'red',
  },
  active: {
    backgroundColor: indigo[500],
    display: 'block',
    textColor: 'white',
    '& *': {
      color: 'white',
    },
  },
});

function HotkeyListItem({ classes, id, title }) {
  return (
    <NavLink
      to={`/hotkeys/${id}`}
      className={classes.link}
      activeClassName={classes.active}
    >
      <ListItem button>
        <ListItemIcon>
          <KeyboardIcon />
        </ListItemIcon>
        <ListItemText primary={title} />
      </ListItem>
    </NavLink>
  );
}

export default withStyles(styles)(HotkeyListItem);
