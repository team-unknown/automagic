import { withStyles } from '@material-ui/core';
import List from '@material-ui/core/List';
import TextField from '@material-ui/core/TextField';
import Fuse from 'fuse.js';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import * as hotkeySelectors from '~app/redux/hotkey/selectors';
import _ from 'lodash';
import HotkeyListItem from './HotkeyListItem';

const search = (list, key) => {
  if (_.isEmpty(key)) {
    return list;
  }

  const options = {
    threshold: 0.6,
    location: 0,
    distance: 100,
    minMatchCharLength: 1,
    keys: ['title'],
  };
  const fuse = new Fuse(list, options);

  return fuse.search(key);
};

const styles = theme => ({
  search: {
    margin: theme.spacing.unit,
  },
});

const DEFAULT_STATE = {
  searchKeyword: null,
};

class HotkeyList extends Component {
  state = DEFAULT_STATE;

  onChange = e => {
    this.setState({
      searchKeyword: e.target.value,
    });
  };

  filteredHotkeys() {
    return search(this.props.hotkeys, this.state.searchKeyword);
  }

  render() {
    const { classes } = this.props;
    const filteredHotkeys = this.filteredHotkeys();
    return (
      <div>
        <TextField
          className={classes.search}
          onChange={this.onChange}
          label="Search"
        />
        <List>
          {filteredHotkeys.map(hotkey => (
            <HotkeyListItem
              key={hotkey.id}
              id={hotkey.id}
              title={hotkey.title}
            />
          ))}
        </List>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  hotkeys: hotkeySelectors.selectHotkeys(state),
});
const mapDispatchToProps = () => ({});

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(HotkeyList);
