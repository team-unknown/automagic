import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import _ from 'lodash';

const MODIFIER = {
  ctrlKey: 'Ctrl',
  altKey: 'Alt',
  shiftKey: 'Shift',
  metaKey: 'Super',
};

const MODIFIER_KEYS = ['CONTROL', 'ALT', 'SHIFT', 'META'];
const hasModifier = e => e.altKey || e.ctrlKey || e.shiftKey || e.metaKey;
const isModifier = key => MODIFIER_KEYS.indexOf(key) >= 0;
const DEFAULT_STATE = {
  currentKey: null,
  modifiers: [],
};

export default class HotkeyRecorder extends React.Component {
  state = DEFAULT_STATE;

  getCurrentCombination() {
    const { currentKey, modifiers } = this.state;

    return _.compact(modifiers.concat(currentKey)).join('+');
  }

  save = () => this.props.onSave(this.getCurrentCombination());

  clearState = () => this.setState(DEFAULT_STATE);

  shouldUpdateCombination = e => {
    const pressedKey = _.toUpper(e.key);

    if (pressedKey === 'ENTER' && !hasModifier(e)) {
      return false;
    }
    if (pressedKey === 'ESCAPE' && !hasModifier(e)) {
      this.clearState();
      this.props.onDiscard();
      return false;
    }
    if (pressedKey === 'BACKSPACE' && !hasModifier(e)) {
      this.clearState();
      return false;
    }

    return true;
  };

  render() {
    return (
      <Dialog
        open
        onClose={this.props.onDiscard}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Record an Hotkey</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Press desired key combination and then press ENTER.
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="key"
            label="Key combination"
            type="text"
            fullWidth
            onKeyDown={e => {
              e.preventDefault();
              e.stopPropagation();

              if (!this.shouldUpdateCombination(e)) {
                return;
              }

              const pressedKey = _.toUpper(e.key);
              const currentKey = isModifier(pressedKey) ? null : pressedKey;

              const modifiers = _.chain(MODIFIER)
                .map((name, key) => (e[key] ? name : null))
                .compact()
                .value();

              this.setState({
                currentKey,
                modifiers,
              });
            }}
            onKeyUp={e => {
              e.preventDefault();
              e.stopPropagation();

              const pressedKey = _.toUpper(e.key);

              if (pressedKey === 'ENTER' && !hasModifier(e)) {
                this.save();
              }
            }}
            value={this.getCurrentCombination()}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={this.props.onDiscard} color="secondary">
            Discard
          </Button>
          <Button onClick={this.save} color="primary">
            Save
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}
